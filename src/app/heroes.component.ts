import { Component } from '@angular/core';
import { Hero } from './hero';
import { OnInit } from '@angular/core';
import {AppComponent} from './app.component';
import { HeroService } from './hero.service';

@Component({
  selector: 'my-heroes',
  templateUrl: './app.component.html',
  // styleUrls: ['./app.component.css'],
})

export class HeroesComponent implements OnInit{
  heroes: Hero[];
  selectedHero: Hero;
 
  
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  ngOnInit(): void {
    this.heroService.getHeroes()
      .then(heroes => this.heroes = heroes.slice(1, 5));
  }
  
}
