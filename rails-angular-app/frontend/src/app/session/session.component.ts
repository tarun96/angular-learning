import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import {User} from '../user';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { AngularTokenService } from 'angular-token';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit {

  constructor(public apiService: ApiService,private flash: FlashMessagesService,public router: Router,tokenService: AngularTokenService) {  }
  
  public user = new User;

  ngOnInit() {
    this.apiService.get("login_status").subscribe((r) =>{
      console.log(r);
      this.tokenService.registerAccount({
        login:                'example@example.org',
        password:             'secretPassword',
        passwordConfirmation: 'secretPassword'
      }).subscribe(
          res =>      console.log(res),
          error =>    console.log(error)
      );
    });
    
  }

  public create(){
    
  }

  public onSubmit(){
    this.apiService.post("auth/sign_in",this.user).subscribe((res)=>{
      if(res){
        console.log(res);
        if(res.status == 200){
          this.router.navigateByUrl('/');
          // this.flash.show("successfully authenticated",{ cssClass: 'alert-success', timeout: 2000 });
        }
        else{
          this.flash.show(res.error,{ cssClass: 'alert-danger', timeout: 2000 });
        }
      }
    });
  }

}
