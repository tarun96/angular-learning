import { Component, OnInit } from '@angular/core';
import {Order} from '../order';
import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-order-add',
  templateUrl: './order-add.component.html',
  styleUrls: ['./order-add.component.css']
})
export class OrderAddComponent implements OnInit {

  constructor(public apiService: ApiService , public acRoute : ActivatedRoute) { }

  public order = new Order;
  product_id:number;

  ngOnInit() {
    // if(this.acRoute.params){
    //   this.acRoute.params.subscribe((data : any)=>{
    //   this.order.product_id = data.id;
    //   this.product_id = data.id;
    //   }
    // }
  }

  public onSubmit(){
    this.apiService.post("orders",this.order).subscribe((r)=>{
    this.order = new Order();
    this.order.product_id = this.product_id;
    alert("Order added !");

    });
  }

}
