import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Product } from '../product';
import { ActivatedRoute } from '@angular/router';
import {Order} from '../order';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {

  constructor(public apiService: ApiService , public acRoute : ActivatedRoute) {
  }

  product = new Product;
  public orders:Array<Order> = [];

  ngOnInit() {
    this.acRoute.params.subscribe((data : any)=>{
    if(data && data.id){
      this.apiService.get_product("product/"+data.id).subscribe((data : Product)=>{
      console.log(data);
        // this.product = data.product;
      // this.orders = data.orders;
      });
    }
    else
    {
      this.product = new Product();
    }
    })
  }

}
