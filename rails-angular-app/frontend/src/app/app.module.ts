import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ApiService} from './api.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ProductAddComponent } from './product-add/product-add.component';
import { ProductListComponent } from './product-list/product-list.component';
import { OrderAddComponent } from './order-add/order-add.component';
import { CustomerAddComponent } from './customer-add/customer-add.component';
import { OrderListComponent } from './order-list/order-list.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ProductViewComponent } from './product-view/product-view.component'; 
import { HttpModule } from '@angular/http';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { SessionComponent } from './session/session.component';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AngularTokenModule } from 'angular-token';
import { AngularTokenService } from 'angular-token';


@NgModule({
  declarations: [AppComponent,ProductAddComponent,ProductListComponent,OrderAddComponent,CustomerAddComponent,OrderListComponent,CustomerListComponent, ProductViewComponent, ToolbarComponent, SessionComponent],
  imports: [BrowserModule,AppRoutingModule,FormsModule,HttpClientModule,HttpModule,FlashMessagesModule.forRoot(),AngularTokenModule.forRoot({}),
    RouterModule.forRoot([
      {path: 'products',component: ProductListComponent},
      {path: 'product/:id',component: ProductViewComponent},
      {path: 'orders',component: OrderListComponent},
      {path: 'customers',component: CustomerListComponent},
      {path: 'products/add',component: ProductAddComponent},
      {path: 'products/add/:id',component: ProductAddComponent},          
      {path: 'orders/add',component: OrderAddComponent},
      {path: 'orders/add/:id',component: OrderAddComponent},          
      {path: 'customers/add',component: CustomerAddComponent},
      {path: 'customers/add/:id',component: CustomerAddComponent},
      {path: 'product/:id/orders/add',component: OrderAddComponent},
      {path: 'sign_in',component: SessionComponent},   
    ]),
  ],
  providers: [ApiService,AngularTokenService,AngularTokenModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
