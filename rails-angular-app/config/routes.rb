Rails.application.routes.draw do
  # mount_devise_token_auth_for 'User', at: 'auth'
  resources :customers
  resources :orders
  resources :products 
  devise_for :users, at: 'auth', controllers: {
      sessions: 'sessions'
  }
  devise_scope :user do
    post "/auth/sign_in" => "sessions#create"
  end
  get "/product/:id" => "products#show"
  get "/login_status" => "application#login_status"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
