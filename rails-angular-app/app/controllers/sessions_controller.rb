class SessionsController < Devise::SessionsController
  
  def create
    user = User.find_by(email: params[:session][:email])
    if user.present?
      if user.valid_password? params[:session][:password]
        sign_in(user)
        render json: {user: user,status:'200'}
      else
        render json: {error: 'Email or password incorrect !',status:'400'}
      end
    else
      render json: {error: 'User not found !',status:'404'}
    end  
  end

end