import { Component, OnInit } from '@angular/core';
import {Angular2TokenService} from 'angular2-token';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.sass']
})
export class AccountComponent implements OnInit {

  constructor( private tokenService: Angular2TokenService ) { 
    this.tokenService.init({
      registerAccountPath: 'http://localhost:3000/auth'
    })
  }

  singUp(){
    alert("ss");
    this.tokenService.registerAccount({
      email: 'tarun@gmail.com',
      password: 'changeit',
      passwordConfirmation: 'changeit'
    }).subscribe(
      res => console.log(res),
      error => console.error()
    );
  }

  ngOnInit() {
  }

}
